
FROM node:14
WORKDIR /home/ubuntu/node-app
COPY . .
RUN npm install
EXPOSE 3000
CMD [ "node", "server.js" ]
